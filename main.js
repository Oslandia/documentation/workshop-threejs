import * as THREE from 'three';
import { MapControls } from 'three/addons/controls/MapControls.js';
import { OBJLoader } from 'three/addons/loaders/OBJLoader.js';
import {MTLLoader} from 'three/addons/loaders/MTLLoader.js';

const zAxis = new THREE.Vector3(0, 0, 1);
// typically, up is z in GIS context
THREE.Object3D.DEFAULT_UP = zAxis;

// create the scene
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 0.1, 1000 );
camera.position.set(-30, -30, 30);
camera.lookAt(0, 0, 0);

// renderer
const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.shadowMap.enabled = true;
document.body.appendChild( renderer.domElement );

const controls = new MapControls( camera, renderer.domElement );

// White directional light at half intensity shining from the top.
const directionalLight = new THREE.DirectionalLight( 0xffffff, 1 );
directionalLight.position.set(0, 10, 70);
directionalLight.target.position.set(0, 0, -10);
directionalLight.castShadow = true;
directionalLight.shadow.camera.left = 50;
directionalLight.shadow.camera.right = -50;
directionalLight.shadow.camera.top = 50;
directionalLight.shadow.camera.bottom = -50;
directionalLight.shadow.mapSize.width = 2048;
directionalLight.shadow.mapSize.height = 2048;

scene.add( directionalLight );
const ambientLight = new THREE.AmbientLight(0xffffff, 0.4);
scene.add(ambientLight);

// add a skybox background
const cubeTextureLoader = new THREE.CubeTextureLoader();
cubeTextureLoader.setPath('./skyboxsun25deg_zup/');
const cubeTexture = cubeTextureLoader.load([
    'px.jpg', 'nx.jpg',
    'py.jpg', 'ny.jpg',
    'pz.jpg', 'nz.jpg',
]);

scene.background = cubeTexture;

// array of obj spec
const objects = [
  {
    geom: new THREE.BoxGeometry(1, 1, 1),
    mat: new THREE.MeshLambertMaterial({ color: 0x00FF00 }),
    position: [0 ,0, 5],
    rotationSpeed: new THREE.Vector2(0.01, 0.01),
    castShadow: true,
    receiveShadow: true
  },
  {
    geom: new THREE.BoxGeometry(1, 2, 1),
    mat: new THREE.MeshLambertMaterial({ color: 0x4925ff }),
    position: [10, 10, 0],
    castShadow: true,
    receiveShadow: true
  },
  {
    geom: new THREE.PlaneGeometry(100, 100),
    mat: new THREE.MeshLambertMaterial({ color: 0xaaaaaa }),
    useTexture: true,
    receiveShadow: true
  },
  {
    geom: new THREE.CapsuleGeometry(3, 10, 30, 30),
    mat: new THREE.MeshLambertMaterial({ color: 0xffFF00 }),
    rotationSpeed: new THREE.Vector2(0.001, 0.001),
    position: [0, 20, 8],
    castShadow: true,
    receiveShadow: true
  },
  {
    geom: new THREE.CircleGeometry( 5, 32 ),
    mat: new THREE.MeshLambertMaterial({ color: 0xc561ff }),
    rotationSpeed: new THREE.Vector2(0.001, 0.001),
    position: [0, 20, 8],
    castShadow: true,
    receiveShadow: true

  },
  {
    geom: new THREE.ConeGeometry( 5, 20, 32 ),
    mat: new THREE.MeshLambertMaterial({ color: 0xb0ffd8 }),
    position: [20, 20, 11],
    rotation: [Math.PI / 2, 0, 0],
    castShadow: true,
    receiveShadow: true
  },
  {
    geom: new THREE.SphereGeometry(5, 20, 32),
    mat: new THREE.MeshLambertMaterial({ color: 0xb0ffd8 }),
    position: [-10, -10, 8],
    castShadow: true,
    receiveShadow: true
  },
  {
    geom: new THREE.BoxGeometry(2, 2),
    mat: new THREE.MeshLambertMaterial({ color: 0xb0d8 }),
    position: [-10, -20, 8],
    rotation: [Math.PI / 2, 0, 0],
    opacity: 0.5,
    castShadow: true,
    receiveShadow: true
  },
];

const texture = new THREE.TextureLoader().load('./mario.png' );

// build objects
for (const obj of objects) {
  const mesh = new THREE.Mesh(obj.geom, obj.mat);
  if (obj.useTexture) {
    mesh.material.map = texture;
  }

  mesh.userData.rotationSpeed = obj.rotationSpeed;
  if (obj.position) {
    mesh.position.fromArray(obj.position);
  }
  if (obj.rotation) {
    mesh.rotation.set(obj.rotation[0], obj.rotation[1], obj.rotation[2]);
  }
  if (obj.opacity) {
    mesh.material.opacity = obj.opacity;
    if (obj.opacity < 1.0) {
      mesh.material.transparent = true;
      mesh.material.side = THREE.DoubleSide;
    }
  }
  mesh.userData.color = mesh.material.color.clone();
  // shadows
  mesh.receiveShadow = obj.receiveShadow;
  mesh.castShadow = obj.castShadow;
  scene.add(mesh);
  obj.mesh = mesh;
}

// load obj
// instantiate a loader
const mtlLoader = new MTLLoader();
mtlLoader.load('./house.mtl', (mtl) => {
  mtl.preload();
  const loader = new OBJLoader();
  loader.setMaterials(mtl);
  // load a resource
  loader.load(
    // resource URL
    './house.obj',
    // called when resource is loaded
    function ( object ) {
      object.rotation.x = Math.PI / 2;
      object.position.x = 100;
      object.traverse(s => {
        if (s.material && s.material.color) {
          s.userData.color = s.material.color.clone();
        }
        // for some reason this mesh has lines instead of meshes sometimes
        if (s.isLine) {
          s.visible = false;
          const mesh = new THREE.Mesh(s.geometry);
          mesh.material = s.material.map(mat => {
            const m = new THREE.MeshLambertMaterial({ color: mat.color})
            if (m.color.r < 0.05 && m.color.g < 0.05 && m.color.b < 0.05) {
              m.color.set(0xcccccc);
            }
            return m;
          });
          object.add(mesh);
        }
      });
      scene.add( object );
      window.house = object;
    });
});
// tidal locked orbit
const earth = objects[6].mesh;
const moon = objects[7].mesh;

const distance = earth.position.distanceTo(moon.position);

// picking
const raycaster = new THREE.Raycaster();
let pointer;

function onPointerMove( event ) {

	// calculate pointer position in normalized device coordinates
	// (-1 to +1) for both components
  if (!pointer) {
    pointer = new THREE.Vector2();
  }

	pointer.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	pointer.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

}

// rendering loop
function animate(t) {
  requestAnimationFrame( animate );
  // animate objects
  scene.traverse(o => {
    if (o.userData.rotationSpeed) {
      o.rotation.x += o.userData.rotationSpeed.x;
      o.rotation.y += o.userData.rotationSpeed.y;
    }

  });
  const angleFrac = (t / 300) % (Math.PI * 2);
  const newPosition = earth.position.clone();
  newPosition.add((new THREE.Vector3(distance, 0, 0)).applyAxisAngle(zAxis, angleFrac));
  moon.position.copy(newPosition);
  moon.rotation.y = angleFrac;

  // update the picking ray with the camera and pointer position
  if (pointer) {
    raycaster.setFromCamera( pointer, camera );

    // calculate objects intersecting the picking ray
    const intersects = raycaster.intersectObjects( scene.children );
    scene.traverse(o => {
      if (o.material && o.material.color) {
        if (intersects.find(i => i.object == o)) {
          o.material.color.set(0x08ff08);
        } else {
          o.material.color.copy(o.userData.color);
        }
        o.material.needsUpdate = true;
      }
    });
  }

  renderer.render( scene, camera );
}
animate();
window.addEventListener( 'pointermove', onPointerMove );
