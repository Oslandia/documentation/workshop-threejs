---
title: "Workshop THREE.js"
tags: bl,itowns,webgl,formation,berger-levrault,workshop
---

# THREE.js workshop

## Getting started

THREE.js a une excellente documentation. Nous allons donc commencer par suivre le [tuto officiel](https://threejs.org/docs/#manual/en/introduction/Installation). 
 
NOTE: vous pouvez choisir la méthode que vous voulez.

Puis continuez avec la [création de notre premiere scène](https://threejs.org/docs/#manual/en/introduction/Creating-a-scene).

## Geometries / materials

Les géométries:

- ajouter d'autres formes géométriques à divers endroits en utilisant les sous-classes de `BufferGeometry`.
- Essayer de construire une forme inédite !

Les materials:

- varier les couleurs, l'opacité etc...
- varier les materials (essayez un qui supporte les éclairages!)
- rendre des objets transparents. Que constatez-vous ?

## Positionnement / Rotation

- Faites bouger un objet en continu
- Faites tourner un objet
- faites orbiter un objet autour d'un point, comme la Lune (en mode tidal lock)
- et les lumières ?

## Controls & picking

- Si ce n'est pas déjà fait, ajouter un contrôle pour vous déplacer dans l'espace
- changer la couleur des objets au clic ou en fonction d'un événement.
    - utiliser le raycasting pour trouver les objets sous la souris
    - changer leur couleur

## Loaders

Charger un [modèle obj](https://free3d.com/fr/3d-model/victorian-building-71236.html):

- le placer
- mettre en avant les éléments au hover
- quand un des éléments est mis en avant, diminuer l'opacité des *autres* éléments

## Gérer la mémoire et les ressources

[Lire ceci une fois dans sa vie](https://threejs.org/docs/#manual/en/introduction/How-to-dispose-of-objects)

- Au click, suppression des objets, appeler dispose()
- vérifier qu'ils sont bien supprimés avec [`renderer.info`](https://threejs.org/docs/index.html?q=renderer#api/en/renderers/WebGLRenderer.info)

## Aller plus loin

- créer son propre matérial implémentant un gradient de couleur par exemple
